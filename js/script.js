$(document).ready(function () {
  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
  });

  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    });
  }
  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });
  //  меню услуг
  $(".js-servise-menu-opener").on("click", function () {
    $(".servise-menu__icon").toggleClass("open");
    $(".js-servise-menu").slideToggle();
  });

  // поиск в шапке
  $(".js-header-search-cross-results").on("click", function () {
    $(".js-header-search-results").hide();
    $(".js-header-search-input").val("");
    if ($(window).width() < 1200) {
      $(".header-search__info").show();
    }
  });
  $(".js-header-search-input").on("input", function () {
    $(".header-search__info").hide();
    $(".js-header-search-results").show();
  });
  $(".js-header-search-open").on("click", function () {
    $(".js-header-search").addClass("open");
    $(".js-header-search-form").addClass("open");
    $(".js-header-search-cross").show();
    $(this).hide();
  });
  $(".js-header-search-cross").on("click", function () {
    $(".js-header-search").removeClass("open");
    $(".js-header-search-form").removeClass("open");
    $(".js-header-search-input").val("");
    $(".js-header-search-open").show();
    $(this).hide();
  });

  // главное меню
  var PriorityNav = {
    menu: $(".js-catalog-menu"),
    allNavElements: $(".js-catalog-menu > ul > li:not('.overflow-nav')"),
    init: function () {
      this.bindUIActions();
      this.setupMenu();
    },
    setupMenu: function () {
      var firstPos = $(".js-catalog-menu > ul > li:first").position();
      var wrappedElements = $();
      var first = true;
      console.log(PriorityNav.allNavElements);
      PriorityNav.allNavElements.each(function (i) {
        var el = $(this);
        var pos = el.position();
        if (pos.top !== firstPos.top) {
          console.log(pos.top, firstPos.top);
          wrappedElements = wrappedElements.add(el);
        }
      });
      if (wrappedElements.length) {
        var newSet = wrappedElements.clone();
        wrappedElements.addClass("hide");
        $(".overflow-nav-list").append(newSet);
        $(".overflow-nav").show();
        $(".js-catalog-menu").css("overflow", "visible");
      }
    },
    tearDown: function () {
      $(".overflow-nav-list").empty();
      $(".overflow-nav").hide();
      PriorityNav.allNavElements.removeClass("hide");
    },
    bindUIActions: function () {
      $(".overflow-nav-title").on("click", function () {
        $(".overflow-nav-list").toggleClass("show");
      });
      $(window)
        .resize(function () {
          PriorityNav.menu.addClass("resizing");
        })
        .resize(function () {
          PriorityNav.tearDown();
          PriorityNav.setupMenu();
          PriorityNav.menu.removeClass("resizing");
        });
    },
  };
  PriorityNav.init();
  // /главное меню
  //  адаптивное меню
  $(".js-mobile-menu-opener").on("click", function () {
    $(".js-mobile-menu-icon").toggleClass("open");
    $(".js-mobile-menu").slideToggle();
    $(".js-fixed-menu-catalog").toggleClass("active");
  });
  //  верхнее меню для адаптива
  $(".js-adaptive-menu-opener").on("click", function () {
    $(".js-adaptive-menu-icon").toggleClass("open");
    $(".js-adaptive-menu").slideToggle();
  });

  $(".js-footer-menu-title").on("click", function () {
    if ($(window).width() < 600) {
      $(this).toggleClass("open");
      $(this)
        .parents(".js-footer-menu")
        .find(".js-footer-menu-ul")
        .slideToggle();
    }
  });

  if ($(".js-txt").length) {
    let text1 = "от производителя";
    let text2 = "в Москве";
    let text = text1;
    let index = 0;
    let isDeleting = false;

    function typeText() {
      let currentText = text.substring(0, index);
      let newText = currentText;
      if (index === currentText.length) {
        newText += "|"; // Add a flashing vertical line at the end of the current text
      }
      $(".js-txt").text(newText);
      if (!isDeleting) {
        index++;
      } else {
        index--;
      }
      if (index > text.length) {
        isDeleting = true;
      }

      if (index === 0) {
        isDeleting = false;
        if (text === text1) {
          text = text2;
        } else {
          text = text1;
        }
      }
      setTimeout(typeText, 200);
    }

    typeText();
  }

  if ($(".js-servises").length) {
    var swiper = new Swiper(".js-servises", {
      slidesPerView: "auto",
      navigation: {
        nextEl: ".js-servises-next",
        prevEl: ".js-servises-prev",
      },

    });
  }

  if ($(".js-catalog-slider").length) {
    var swiper = new Swiper(".js-catalog-slider", {
      slidesPerView: "auto",
      navigation: {
        nextEl: ".js-catalog-slider-next",
        prevEl: ".js-catalog-slider-prev",
      },
      
    });
  }

  if ($(".js-partners").length) {
    var swiper = new Swiper(".js-partners", {
      slidesPerView: "auto",
      navigation: {
        nextEl: ".js-partners-next",
        prevEl: ".js-partners-prev",
      },
  
    });
  }

  $(".js-text-block-show").on("click", function () {
    $(".js-text-block").addClass("open");
  });
  $(".js-text-block-hide").on("click", function () {
    $(".js-text-block").removeClass("open");
  });

  if ($(".js-map").length) {
    ymaps.ready(function () {
      var myMap = new ymaps.Map(
          "map",
          {
            center: [55.348815, 38.009382],
            zoom: 17,
            controls: [],
          },
          {
            suppressMapOpenBlock: true
          }
        ),
        myPlacemark = new ymaps.Placemark(
          [55.348815, 38.009382],
          {
            hintContent: "Собственный значок метки",
            balloonContent: "Это красивая метка",
          },
          {
            iconLayout: "default#image",
            iconImageHref: "D:/фронтенд/строймонолит/img/icons/point.svg",
            iconImageSize: [70, 109],
            iconImageOffset: [-35, -109],
          }
        );

      myMap.geoObjects.add(myPlacemark);
      myMap.behaviors.disable("scrollZoom");
    });
  }

  $('.js-top-block-show').on("click", function () {
    $('.js-top-block-txt').addClass("open");
    $('.js-top-block-hide').show()
    $(this).hide()
  })
  $('.js-top-block-hide').on("click", function () {
    $('.js-top-block-txt').removeClass("open");
    $('.js-top-block-show').show()
    $(this).hide()
  })

  function initProductSwiper(swiper) {
    var slider = swiper.find(".js-product-slider")
      var thumbs = swiper.find(".js-product-slider-thumbs")
      var next = swiper.find(".js-product-slider-next")
      var prev = swiper.find(".js-product-slider-prev")


        var swiper1 = new Swiper(thumbs, {
          spaceBetween: 10,
          slidesPerView: 'auto',
          watchSlidesProgress: true,
          direction: "vertical",

          navigation: {
            nextEl: next,
            prevEl: prev,
          },
          breakpoints: {
            0: {
              direction: "horizontal",
            },
            599: {
              direction: "horizontal",
            },
            600: {
              direction: "vertical",
            },
          }
        });
        var swiper2 = new Swiper(slider, {
          loop: true,
          spaceBetween: 24,
          navigation: {
            nextEl: next,
            prevEl: prev,
          },
          thumbs: {
            swiper: swiper1,
          },
        });
  }

  if ($(".js-product-slider").length) {
    $(".product-slider:first-child").addClass('active')
    initProductSwiper( $(".product-slider:first-child"))
  }
  $('.product-page__img').on("click", function (e) {
    e.preventDefault()
    let id = $(this).attr('href')
    console.log(id)
    $(".product-slider.active").removeClass('active')
    $(id).addClass('active')
    $(".product-page__img.active").removeClass('active')
    $(this).addClass('active')
    initProductSwiper( $(id))
  })

  $('.js-counter-add').on("click", function () {
    let val = $(this).parents('.js-counter').find('.js-counter-field').val()
    val++
    $(this).parents('.js-counter').find('.js-counter-field').val(val)
    if($('.js-product-page-price').length) {
      price=parseInt($('.js-product-page-price').text())
      let summ = (price*val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      $('.js-product-page-summ').text(summ)
    }
  })
  $('.js-counter-remove').on("click", function () {
    let val = $(this).parents('.js-counter').find('.js-counter-field').val()
    val--
    if(val < 0) {
      val = 0
    }
    if($('.js-product-page-price').length) {
      price=parseInt($('.js-product-page-price').text())
      let summ = (price*val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      $('.js-product-page-summ').text(summ)
    }
    $(this).parents('.js-counter').find('.js-counter-field').val(val)
  })
  $('.js-counter-field').on("change", function () {
    let val = $(this).val()
    if(val < 0 || isNaN(parseInt(val))) {
      val = 0
    }
    if($('.js-product-page-price').length) {
      price=parseInt($('.js-product-page-price').text())
      let summ = (price*val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
      $('.js-product-page-summ').text(summ)
    }
    $(this).val(val)
  })

  $('.js-tabs-link').on('click', function(e) {
    e.preventDefault()
    let val = $(this).attr('href')
    if (!$(this).hasClass('active')) {
      $('.js-tabs-link.active').removeClass('active')
      $('.js-tabs-content.active').removeClass('active').hide()
    }
    $(this).addClass('active')
    $(val).show().addClass('active')
  })
  if($('.js-tabs-link').length) {
    $('.tabs__item:first-child .js-tabs-link').addClass('active')
    let val = $('.tabs__item:first-child .js-tabs-link').attr('href')
    $(val).show().addClass('active')
  }
  if($('.js-tabs-link').length) {
    $('.order__tabs .js-tabs-link:first-child').addClass('active')
    let val = $('.order__tabs .js-tabs-link:first-child').attr('href')
    $(val).show().addClass('active')
  }

  if ($('.js-scroll-menu').length) {
    $(window).on('scroll', function() {
      var scrollPos = $(window).scrollTop();
      
      $('.section').each(function() {
        var offsetTop = $(this).offset().top - 150;
        var outerHeight = $(this).outerHeight();

        if (scrollPos >= offsetTop && scrollPos < offsetTop + outerHeight) {
          var sectId = $(this).attr('id');
          $('.left-menu__link').removeClass('active');
          $('.left-menu__link[href="#' + sectId + '"]').addClass('active');
        }
      });
    });

    $('.js-scroll-menu a[href^="#"]').on('click', function(e) {
      e.preventDefault();
      var id = $(this).attr('href');
      $('body,html').animate({
        scrollTop: $(id).offset().top - 100
      });
    });
  }

 
});
